var Counter = function(textAreaId,targetDivId,param){
	
	var CounterObject = {
			
		textArea : document.getElementById(textAreaId),
		targetDiv : document.getElementById(targetDivId),
		
		overSizeColor : "red",
		normalColor : "black",
		elementTypeForCounter : "P",
		separator : "/",
		newLineCharValue : 2,
		maxLength : null,
		actualLength : null,
		node : null,
		textnode : null,
		
		node : document.createElement(this.elementTypeForCounter),
		
		lineFeedCount : function(){
			var newValue = this.textArea.value;
			var lineBreaks = newValue.split(/\r\n|\r|\n/).length - 1;
			var count = this.textArea.value.length - lineBreaks + (lineBreaks * this.newLineCharValue);
			
			return count;
		},
		
		verifyLength : function(text){	
			if(text > this.maxLength){
				this.node.style.color = this.overSizeColor
			}else{
				this.node.style.color = this.normalColor
			}
		},
	
		onChange  : function(){
			var newValue = this.lineFeedCount();
			this.textnode.nodeValue = newValue + this.separator + this.maxLength;
			
			this.node.appendChild(this.textnode);
			this.targetDiv.appendChild(this.node);
			
			this.verifyLength(newValue);
		},
		
		create : function(){
			var that = this;
			this.maxLength = this.maxLength != null ? this.maxLength : this.textArea.getAttribute("maxlength");
			this.actualLength = this.textArea.value.length;
			this.textnode = document.createTextNode(this.actualLength + " / " + this.maxLength);
			this.node.appendChild(this.textnode);
			this.targetDiv.appendChild(this.node);
			
			this.textArea.onpaste = function(){
				setTimeout(function(){ that.onChange(); }, 1);
			}
			this.textArea.onkeydown = function(){
				setTimeout(function(){ that.onChange(); }, 1);
			}
			this.textArea.onkeyup = function(){
				setTimeout(function(){ that.onChange(); }, 1);
			}

			this.verifyLength();	
		}
		
	}
	
	$.extend(CounterObject,param);
	CounterObject.create();
	return CounterObject;
}